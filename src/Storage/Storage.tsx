import {isPresent, toJsString} from "../util/util";

const DEFAULT_KEY_PREFIX = "";

type StorageContainer = globalThis.Storage;

class Storage {
  static local: (keyPrefix?: string) => Storage;
  static session: (keyPrefix?: string) => Storage;
  private container: StorageContainer;
  private keyPrefix: string;

  constructor(container: StorageContainer, keyPrefix?: string) {
    this.container = container;
    this.keyPrefix = keyPrefix || DEFAULT_KEY_PREFIX;
  }

  get<T>(key: string, defaultValue: T | undefined = undefined): T | string | undefined {
    if (!this.hasKey(key)) return defaultValue;
    const value = this.container.getItem(this.prefixedKey(key)) || "";
    try {
      return JSON.parse(value);
    } catch (e) {
      return value;
    }
  }

  set(key: string | null | undefined, value: any): void {
    if (typeof key === 'string' && isPresent(value)) {
      this.addItem(this.prefixedKey(key), toJsString(value));
    }
  }

  keys(): string[] {
    return Object.keys(this.container);
  }

  size(): number {
    return this.container.length;
  }

  hasKey(key: string): boolean {
    return this.container.hasOwnProperty(this.prefixedKey(key));
  }

  delete(key: string): void {
    this.container.removeItem(this.prefixedKey(key));
  }

  clear(): void {
    return this.container.clear();
  }

  prefixedKey(key: string): string {
    return this.keyPrefix.length > 0 ? `${this.keyPrefix}.${key}` : key.toString();
  }

  private addItem(key: string, value: string | number): void {
    this.container.setItem(key, value.toString());
  }
}

Storage.local = (keyPrefix: string = DEFAULT_KEY_PREFIX) => new Storage(localStorage, keyPrefix);

Storage.session = (keyPrefix: string = DEFAULT_KEY_PREFIX) => new Storage(sessionStorage, keyPrefix);

export default Storage;

export { Storage };

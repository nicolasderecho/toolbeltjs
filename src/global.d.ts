import 'jest-fetch-mock'
import './types'

declare global {
    function raisePromiseShouldHaveFailedError(): never;
}
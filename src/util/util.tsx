const extractToken = (rawToken: string): string => {
  const values: string[] = rawToken.split("Bearer");
  return (values[1] || values[0] || rawToken || "").trim();
};

const isPresent = (value: unknown): boolean => value !== null && value !== undefined;

const isJsonString = (value: any): boolean => {
    try {
        JSON.parse(value);
    } catch (e) {
        return false;
    }
    return true;
}

const toJsString = (value: any): string => isJsonString(value) || typeof value === 'string' ? value : JSON.stringify(value);

export { extractToken, isPresent, isJsonString, toJsString };

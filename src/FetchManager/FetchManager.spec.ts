//http://api.tvmaze.com/search/shows?q=the+boys
import { FetchManager } from "./FetchManager";
import {
    stubNetworkError,
    stubGetRequest,
    stubPostRequest,
    server,
    stubPutRequest,
    stubPatchRequest, stubDeleteRequest
} from "../../support/fakeServer";
import {rest} from "msw";

it("can stub http requests with ease", async () => {
    stubGetRequest('http://localhost:3000/zarlanga', { rates: { CAD: 1.42 } })
    const result = await fetch("http://localhost:3000/zarlanga");
    const rate = await result.json();
    expect(rate.rates.CAD).toEqual(1.42);
    expect(fetch).toHaveBeenCalledTimes(1);
});

type Dog = {
    name: string;
    size: string;
};

let fetchManager = new FetchManager();

describe("GET requests", () => {

    it('makes a simple GET request', async() => {
        const url = 'https://dogs.com';
        const dogs: Dog[] = [{name: 'Bulldog', size: 'medium'},{name: 'Poodle', size: 'small'}];
        stubGetRequest(url, dogs);
        const result: HttpRequestResponse<Dog[]> = await fetchManager.get(url);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(dogs);
    });

});

describe("POST requests", () => {
    it('makes a simple POST request', async() => {
        const url = 'https://dogs.com/create';
        const body = {name: 'Hot dog', size: 'Yummy'};
        stubPostRequest(url, body, {code: 201});
        const result = await fetchManager.post<Dog>(url, body);
        expect(result.status).toEqual(201);
        expect(result.data).toEqual(body);
    });
});

describe("PUT requests", () => {
    it('makes a simple PUT request', async() => {
        const url = 'https://dogs.com/10';
        const body = {name: 'Tenth dog', size: 'Yummy'};
        stubPutRequest(url, body, {code: 200});
        const result = await fetchManager.put<Dog>(url, body);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(body);
    });
});

describe("PATCH requests", () => {
    it('makes a simple PATCH request', async() => {
        const url = 'https://dogs.com/10';
        const body = {name: 'Tenth dog'};
        stubPatchRequest(url, body, {code: 200});
        const result = await fetchManager.patch<Dog>(url, body);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(body);
    });
});

describe("DELETE requests", () => {
    it('makes a simple DELETE request', async() => {
        const url = 'https://dogs.com/10';
        stubDeleteRequest(url);
        const result = await fetchManager.delete<Dog>(url);
        expect(result.status).toEqual(200);
    });
});

describe('Request', () => {

    it('makes a request with the specified method', async() => {
        const url = 'https://dogs.com';
        const dogs: Dog[] = [{name: 'Bulldog', size: 'medium'},{name: 'Poodle', size: 'small'}];
        stubGetRequest(url, dogs);
        const result: HttpRequestResponse<Dog[]> = await fetchManager.request('GET', url);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(dogs);
    });

    it('fails when the response is a 4xx', async () => {
        const url = 'https://dogs.com/tyrannosaurus';
        stubGetRequest(url, {message: 'Page not found'}, {code: 404 });
        try {
            await fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch(error) {
            expect(error.status).toEqual(404);
            expect(error.data.message).toEqual('Page not found');
        }
    });

    it('fails when the response is a 5xx', async () => {
        const url = 'https://dogs.com/error';
        stubGetRequest(url, {message: 'Bad Gateway'}, {code: 502 });
        try {
            await fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch(error) {
            expect(error.status).toEqual(502);
            expect(error.data.message).toEqual('Bad Gateway');
        }
    });

    it('fails when the response is a network failure', async () => {
        const url = 'https://dogs.com';
        stubNetworkError(url);
        try {
            await fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch(error) {
            if(! (error instanceof Error) ) throw('unexpected Error received');
            expect(error.message).toEqual("Network request failed");
        }
    });

    it('can be aborted', (done) => {
        const controller = new AbortController();
        const url = 'https://dogs.com';
        const dogs: Dog[] = [{name: 'Bulldog', size: 'medium'},{name: 'Poodle', size: 'small'}];
        stubGetRequest(url, dogs);
        fetchManager.request('GET', url, {signal: controller.signal})
                    .then(() => fail('this request should have been aborted'))
                    .catch((error) => expect(error.name).toEqual('AbortError') )
                    .finally(done);
        controller.abort();
    });

    describe('Query params', () => {
        it('supports object query params', async() => {
            const url = 'https://dogs.com';
            const dogs: Dog[] = [{name: 'Bulldog', size: 'medium'},{name: 'Poodle', size: 'small'}];
            server.use(
                rest.get(url, (req, res, ctx) => {
                    const param1 = req.url.searchParams.get('param1');
                    const param2 = req.url.searchParams.get('param2');
                    return res(
                        (param1 === '1' && param2 === '2') ? ctx.json(dogs) : ctx.json([])
                    )
                })
            )
            const result: HttpRequestResponse<Dog[]> = await fetchManager.request('GET', url, {params: {param1: 1, param2: 2}});
            expect(result.status).toEqual(200);
            expect(result.data).toEqual(dogs);
        });
    });

    describe('interceptors', () => {
        const url = 'https://dogs.com';
        const interceptor = jest.fn((value) => value);
        const secondInterceptor = jest.fn((value) => value);

        beforeEach(() => stubPostRequest(url, {}));

        it('supports multiple interceptors before making the request', async () => {
            fetchManager.beforeRequest(interceptor);
            fetchManager.beforeRequest(secondInterceptor);
            await fetchManager.post(url, {});
            expect(interceptor).toHaveBeenCalled();
            expect(secondInterceptor).toHaveBeenCalled();
        })
    })
});
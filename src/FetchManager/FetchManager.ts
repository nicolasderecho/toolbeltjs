const generateFetchJsonResponse = async (response: Response, resolve: Function, reject: Function) => {
    const data = await response.json();
    const requestResponse: HttpRequestResponse<typeof data> = {
        headers: response.headers,
        status: response.status,
        data
    };
    if(response.ok) {
        resolve(requestResponse);
    } else {
        reject(requestResponse);
    }
}

const buildUrl = (host: string, path: string, params: object): string => {
    const url = new URL(`${host}${path}`);
    // @ts-ignore
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    return url.toString();
}

type FetchManagerParams = RequestInit & {
    host?: string;
};

type RequestOptions = RequestInit & {
    params?: object;
}

type RequestMethod = 'GET' | 'POST' | 'OPTIONS' | 'DELETE' | 'PUT' | 'PATCH' | 'HEAD';

type InterceptorFunction = <T,>(value: T ) => T | Promise<T>;

const IdentityFunction = <T,>(value: T ): T => value;

class FetchManager {
    public requestConfig: RequestInit;
    public host: string;
    private interceptorsBeforeRequest: InterceptorFunction[];

    constructor(options : FetchManagerParams = {}) {
        this.interceptorsBeforeRequest = [];
        this.requestConfig = {
            cache: options.cache || 'no-cache',
            credentials: options.credentials || 'same-origin',
            headers: options.headers || {},
            integrity: options.integrity,
            keepalive: Object.keys(options).includes('keepalive') ? options.keepalive : false,
            mode: options.mode || 'cors',
            redirect: options.redirect || 'follow',
            referrer: Object.keys(options).includes('referrer') ? options.referrer : 'about:client',
            referrerPolicy: options.referrerPolicy || 'no-referrer-when-downgrade',
            signal: options.signal || undefined,
            window: options.window || window
        }
        this.host = options.host || '';
    }

    beforeRequest(functionToCall: InterceptorFunction) {
        this.interceptorsBeforeRequest = this.interceptorsBeforeRequest.concat(functionToCall);
    }

    options<T>(path: string, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        return this.request('OPTIONS', path, options);
    }

    head<T>(path: string, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        return this.request('HEAD', path, options);
    }

    get<T>(path: string, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        return this.request('GET', path, options);
    }

    delete<T>(path: string, body?: object | null, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        const requestOptions: RequestOptions = Object.assign({}, options, {body: body ? JSON.stringify(body) : undefined } );
        return this.request('DELETE', path, requestOptions);
    }

    post<T>(path: string, body: object, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        const requestOptions: RequestOptions = Object.assign({}, options, {body: JSON.stringify(body)});
        return this.request('POST', path, requestOptions);
    }

    put<T>(path: string, body: object, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        const requestOptions: RequestOptions = Object.assign({}, options, {body: JSON.stringify(body)});
        return this.request('PUT', path, requestOptions);
    }

    patch<T>(path: string, body: object, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        const requestOptions: RequestOptions = Object.assign({}, options, {body: JSON.stringify(body)});
        return this.request('PATCH', path, requestOptions);
    }

    async request <T>(method: RequestMethod, path: string, options: RequestOptions = {}): Promise<HttpRequestResponse<T>> {
        const { params = {}, ...fetchOptions } = options;
        const config: RequestInit = Object.assign({}, this.requestConfig, fetchOptions);
        const configAfterInterceptors = this.applyBeforeInterceptors(config);
        const requestOptions: RequestInit = Object.assign({}, configAfterInterceptors, {method: method});
        return new Promise((resolve: (value: HttpRequestResponse<T>) => void, reject: (error: HttpRequestResponse<unknown> | Error) => void) => {
            fetch(buildUrl(this.host, path, params), requestOptions)
                .then( (response: Response) => generateFetchJsonResponse(response, resolve, reject))
                .catch(reject);
        });
    }

    private asPromise<T>(value: T | Promise<T>): Promise<T> {
        return Promise.resolve(value);
    }

     private applyBeforeInterceptors(initialConfig: RequestInit): RequestInit {
        let config = initialConfig;
        this.interceptorsBeforeRequest.forEach( async (functionToCall: InterceptorFunction) => {
            config = await this.asPromise(functionToCall(config));
        });
        return config;
    }
}

export default FetchManager;

export {FetchManager}
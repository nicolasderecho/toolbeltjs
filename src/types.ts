interface HttpStatus {
    code: number;
    text?: string;
}

type HttpMethod = 'get' | 'post' | 'options' | 'delete' | 'put' | 'patch' | 'head';

interface HttpRequestResponse<T> {
    status: number;
    headers: Headers;
    data: T;
}
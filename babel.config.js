module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
    [
      "@babel/plugin-proposal-object-rest-spread"
    ]
  ],
};
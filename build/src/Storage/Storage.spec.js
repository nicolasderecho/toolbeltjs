import { Storage } from "./Storage";
const storage = new Storage(localStorage);
beforeEach(() => localStorage.clear());
describe("set", () => {
    it('adds a new item to the storage', () => {
        storage.set('1', 10);
        expect(localStorage.getItem('1')).toEqual('10');
    });
    it('stringifies the item to add', () => {
        storage.set('2', { a: 20 });
        expect(localStorage.getItem('2')).toEqual('{"a":20}');
    });
    it("doesn't stringify the item when it's a valid JSON string", () => {
        storage.set('2', '{"a":10}');
        storage.set('3', 'some-string');
        expect(localStorage.getItem('2')).toEqual('{"a":10}');
        expect(localStorage.getItem('3')).toEqual('some-string');
    });
});
describe("get", () => {
    it('returns the value stored in the storage', () => {
        storage.set('a-key', 'value');
        expect(storage.get('a-key')).toEqual('value');
    });
    it('accepts a default value to be returned when the key is not found', () => {
        expect(storage.get('unknown-key', 20)).toEqual(20);
    });
});
describe('keys', () => {
    it('returns the stored keys', () => {
        storage.set('color', 'red');
        storage.set('size', 'big');
        expect(storage.keys()).toContain('color');
        expect(storage.keys()).toContain('size');
    });
});
describe('size', () => {
    it('returns the number of stored keys', () => {
        expect(storage.size()).toEqual(0);
        storage.set('color', 'red');
        expect(storage.size()).toEqual(1);
        storage.set('size', 'big');
        expect(storage.size()).toEqual(2);
    });
});
describe('hasKey', () => {
    it('returns true when the received key is stored', () => {
        storage.set('color', 'blue');
        expect(storage.hasKey('color')).toBeTruthy();
    });
    it('returns false when the received key is not stored', () => {
        expect(storage.hasKey('color')).toBeFalsy();
    });
});
describe('delete', () => {
    it('removes an entry from the storage', () => {
        storage.set('color', 'red');
        expect(storage.hasKey('color')).toBeTruthy();
        storage.delete('color');
        expect(storage.hasKey('color')).toBeFalsy();
    });
});
describe('clear', () => {
    it('removes all entries from the storage', () => {
        storage.set('color', 'red');
        storage.set('size', 'big');
        expect(storage.size()).toEqual(2);
        storage.clear();
        expect(storage.size()).toEqual(0);
    });
});
//# sourceMappingURL=Storage.spec.js.map
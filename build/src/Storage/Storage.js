import { isPresent, toJsString } from "../util/util";
const DEFAULT_KEY_PREFIX = "";
class Storage {
    constructor(container, keyPrefix) {
        this.container = container;
        this.keyPrefix = keyPrefix || DEFAULT_KEY_PREFIX;
    }
    get(key, defaultValue = undefined) {
        if (!this.hasKey(key))
            return defaultValue;
        const value = this.container.getItem(this.prefixedKey(key)) || "";
        try {
            return JSON.parse(value);
        }
        catch (e) {
            return value;
        }
    }
    set(key, value) {
        if (typeof key === 'string' && isPresent(value)) {
            this.addItem(this.prefixedKey(key), toJsString(value));
        }
    }
    keys() {
        return Object.keys(this.container);
    }
    size() {
        return this.container.length;
    }
    hasKey(key) {
        return this.container.hasOwnProperty(this.prefixedKey(key));
    }
    delete(key) {
        this.container.removeItem(this.prefixedKey(key));
    }
    clear() {
        return this.container.clear();
    }
    prefixedKey(key) {
        return this.keyPrefix.length > 0 ? `${this.keyPrefix}.${key}` : key.toString();
    }
    addItem(key, value) {
        this.container.setItem(key, value.toString());
    }
}
Storage.local = (keyPrefix = DEFAULT_KEY_PREFIX) => new Storage(localStorage, keyPrefix);
Storage.session = (keyPrefix = DEFAULT_KEY_PREFIX) => new Storage(sessionStorage, keyPrefix);
export default Storage;
export { Storage };
//# sourceMappingURL=Storage.js.map
declare type StorageContainer = globalThis.Storage;
declare class Storage {
    static local: (keyPrefix?: string) => Storage;
    static session: (keyPrefix?: string) => Storage;
    private container;
    private keyPrefix;
    constructor(container: StorageContainer, keyPrefix?: string);
    get<T>(key: string, defaultValue?: T | undefined): T | string | undefined;
    set(key: string | null | undefined, value: any): void;
    keys(): string[];
    size(): number;
    hasKey(key: string): boolean;
    delete(key: string): void;
    clear(): void;
    prefixedKey(key: string): string;
    private addItem;
}
export default Storage;
export { Storage };

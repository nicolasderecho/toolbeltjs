interface HttpStatus {
    code: number;
    text?: string;
}
declare type HttpMethod = 'get' | 'post' | 'options' | 'delete' | 'put' | 'patch' | 'head';
interface HttpRequestResponse<T> {
    status: number;
    headers: Headers;
    data: T;
}

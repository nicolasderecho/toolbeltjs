var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
//http://api.tvmaze.com/search/shows?q=the+boys
import { FetchManager } from "./FetchManager";
import { stubNetworkError, stubGetRequest, stubPostRequest, server, stubPutRequest, stubPatchRequest, stubDeleteRequest } from "../../support/fakeServer";
import { rest } from "msw";
it("can stub http requests with ease", () => __awaiter(void 0, void 0, void 0, function* () {
    stubGetRequest('http://localhost:3000/zarlanga', { rates: { CAD: 1.42 } });
    const result = yield fetch("http://localhost:3000/zarlanga");
    const rate = yield result.json();
    expect(rate.rates.CAD).toEqual(1.42);
    expect(fetch).toHaveBeenCalledTimes(1);
}));
let fetchManager = new FetchManager();
describe("GET requests", () => {
    it('makes a simple GET request', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com';
        const dogs = [{ name: 'Bulldog', size: 'medium' }, { name: 'Poodle', size: 'small' }];
        stubGetRequest(url, dogs);
        const result = yield fetchManager.get(url);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(dogs);
    }));
});
describe("POST requests", () => {
    it('makes a simple POST request', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/create';
        const body = { name: 'Hot dog', size: 'Yummy' };
        stubPostRequest(url, body, { code: 201 });
        const result = yield fetchManager.post(url, body);
        expect(result.status).toEqual(201);
        expect(result.data).toEqual(body);
    }));
});
describe("PUT requests", () => {
    it('makes a simple PUT request', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/10';
        const body = { name: 'Tenth dog', size: 'Yummy' };
        stubPutRequest(url, body, { code: 200 });
        const result = yield fetchManager.put(url, body);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(body);
    }));
});
describe("PATCH requests", () => {
    it('makes a simple PATCH request', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/10';
        const body = { name: 'Tenth dog' };
        stubPatchRequest(url, body, { code: 200 });
        const result = yield fetchManager.patch(url, body);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(body);
    }));
});
describe("DELETE requests", () => {
    it('makes a simple DELETE request', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/10';
        stubDeleteRequest(url);
        const result = yield fetchManager.delete(url);
        expect(result.status).toEqual(200);
    }));
});
describe('Request', () => {
    it('makes a request with the specified method', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com';
        const dogs = [{ name: 'Bulldog', size: 'medium' }, { name: 'Poodle', size: 'small' }];
        stubGetRequest(url, dogs);
        const result = yield fetchManager.request('GET', url);
        expect(result.status).toEqual(200);
        expect(result.data).toEqual(dogs);
    }));
    it('fails when the response is a 4xx', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/tyrannosaurus';
        stubGetRequest(url, { message: 'Page not found' }, { code: 404 });
        try {
            yield fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch (error) {
            expect(error.status).toEqual(404);
            expect(error.data.message).toEqual('Page not found');
        }
    }));
    it('fails when the response is a 5xx', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com/error';
        stubGetRequest(url, { message: 'Bad Gateway' }, { code: 502 });
        try {
            yield fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch (error) {
            expect(error.status).toEqual(502);
            expect(error.data.message).toEqual('Bad Gateway');
        }
    }));
    it('fails when the response is a network failure', () => __awaiter(void 0, void 0, void 0, function* () {
        const url = 'https://dogs.com';
        stubNetworkError(url);
        try {
            yield fetchManager.request('GET', url);
            raisePromiseShouldHaveFailedError();
        }
        catch (error) {
            if (!(error instanceof Error))
                throw ('unexpected Error received');
            expect(error.message).toEqual("Network request failed");
        }
    }));
    it('can be aborted', (done) => {
        const controller = new AbortController();
        const url = 'https://dogs.com';
        const dogs = [{ name: 'Bulldog', size: 'medium' }, { name: 'Poodle', size: 'small' }];
        stubGetRequest(url, dogs);
        fetchManager.request('GET', url, { signal: controller.signal })
            .then(() => fail('this request should have been aborted'))
            .catch((error) => expect(error.name).toEqual('AbortError'))
            .finally(done);
        controller.abort();
    });
    describe('Query params', () => {
        it('supports object query params', () => __awaiter(void 0, void 0, void 0, function* () {
            const url = 'https://dogs.com';
            const dogs = [{ name: 'Bulldog', size: 'medium' }, { name: 'Poodle', size: 'small' }];
            server.use(rest.get(url, (req, res, ctx) => {
                const param1 = req.url.searchParams.get('param1');
                const param2 = req.url.searchParams.get('param2');
                return res((param1 === '1' && param2 === '2') ? ctx.json(dogs) : ctx.json([]));
            }));
            const result = yield fetchManager.request('GET', url, { params: { param1: 1, param2: 2 } });
            expect(result.status).toEqual(200);
            expect(result.data).toEqual(dogs);
        }));
    });
    describe('interceptors', () => {
        const url = 'https://dogs.com';
        const interceptor = jest.fn((value) => value);
        const secondInterceptor = jest.fn((value) => value);
        beforeEach(() => stubPostRequest(url, {}));
        it('supports multiple interceptors before making the request', () => __awaiter(void 0, void 0, void 0, function* () {
            fetchManager.beforeRequest(interceptor);
            fetchManager.beforeRequest(secondInterceptor);
            yield fetchManager.post(url, {});
            expect(interceptor).toHaveBeenCalled();
            expect(secondInterceptor).toHaveBeenCalled();
        }));
    });
});
//# sourceMappingURL=FetchManager.spec.js.map
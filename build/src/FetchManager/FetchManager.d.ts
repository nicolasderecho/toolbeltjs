declare type FetchManagerParams = RequestInit & {
    host?: string;
};
declare type RequestOptions = RequestInit & {
    params?: object;
};
declare type RequestMethod = 'GET' | 'POST' | 'OPTIONS' | 'DELETE' | 'PUT' | 'PATCH' | 'HEAD';
declare type InterceptorFunction = <T>(value: T) => T | Promise<T>;
declare class FetchManager {
    requestConfig: RequestInit;
    host: string;
    private interceptorsBeforeRequest;
    constructor(options?: FetchManagerParams);
    beforeRequest(functionToCall: InterceptorFunction): void;
    options<T>(path: string, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    head<T>(path: string, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    get<T>(path: string, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    delete<T>(path: string, body?: object | null, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    post<T>(path: string, body: object, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    put<T>(path: string, body: object, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    patch<T>(path: string, body: object, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    request<T>(method: RequestMethod, path: string, options?: RequestOptions): Promise<HttpRequestResponse<T>>;
    private asPromise;
    private applyBeforeInterceptors;
}
export default FetchManager;
export { FetchManager };

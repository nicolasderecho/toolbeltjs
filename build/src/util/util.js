const extractToken = (rawToken) => {
    const values = rawToken.split("Bearer");
    return (values[1] || values[0] || rawToken || "").trim();
};
const isPresent = (value) => value !== null && value !== undefined;
const isJsonString = (value) => {
    try {
        JSON.parse(value);
    }
    catch (e) {
        return false;
    }
    return true;
};
const toJsString = (value) => isJsonString(value) || typeof value === 'string' ? value : JSON.stringify(value);
export { extractToken, isPresent, isJsonString, toJsString };
//# sourceMappingURL=util.js.map
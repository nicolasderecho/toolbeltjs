declare const extractToken: (rawToken: string) => string;
declare const isPresent: (value: unknown) => boolean;
declare const isJsonString: (value: any) => boolean;
declare const toJsString: (value: any) => string;
export { extractToken, isPresent, isJsonString, toJsString };

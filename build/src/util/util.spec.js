import { extractToken, isJsonString, isPresent, toJsString } from "./util";
describe("extractToken", () => {
    const token = "eyJraWQiOiIxYWI2YjBhNjAxNTY0MmY4YjA5MWM3MjE1MWIxMGQ5MCIsImFsZyI6IkVTMjU2In0.eyJpc3MiOiJodHRwczovL3d3dy50YWxrZGVza2lkLmNvbSIsImF1ZCI6Imh0dHBzOi8vYXBpLnRhbGtkZXNrYXBwLmNvbSIsImV4cCI6MTU5ODAyMjU4OCwibmJmIjoxNTk4MDIxOTg4LCJpYXQiOjE1OTgwMjE5ODgsImp0aSI6ImEwZGVkOWI2N2NmMDQ4ZjM5MDdiMzQ5YmE0NDg3M2I0IiwiY2lkIjoiNDM3ZmMxNDIwZmQ5NDMyZmIwMWZlMTg0NjI5NmY5YWYiLCJndHkiOiJjbGllbnRfY3JlZGVudGlhbHMiLCJzY3AiOlsib3BlbmlkIiwidXNlcnM6cmVhZCIsImV2ZW50czpyZWFkIiwiYXBwczpyZWFkIiwiYXBwczp3cml0ZSIsInJlcG9ydHM6cmVhZCIsImFjY291bnQ6cmVhZCIsImNhbGxiYWNrOndyaXRlIiwic3R5eC1xdWVyaWVzOnJlYWQiLCJzdHl4LXN1YnNjcmlwdGlvbnM6cmVhZCIsInN0eXgtc3Vic2NyaXB0aW9uczp3cml0ZSJdLCJybG0iOiJNQUlOIiwiYWlkIjoiNWVjMmI5MzcwZGU2YTAwMDAxMGNjNjY1IiwiYWNjIjoiaG9vcGxhIiwic3ViIjoiNDM3ZmMxNDIwZmQ5NDMyZmIwMWZlMTg0NjI5NmY5YWYifQ.V9oPtMt14AnbeHEBNCDzOrV7WVOuIt12geRt-DD0NXomjf0QyzO3vubUhWCDCXM0yKTsfoHEe0Or8xahmf8UGw";
    it("extracts the token from a typical Authorization header structure", () => {
        expect(extractToken(`Bearer ${token}`)).toEqual(token);
    });
    it("returns the received string if it can't find the 'Bearer' key", () => {
        expect(extractToken(token)).toEqual(token);
    });
});
describe("isPresent", () => {
    it("returns false when the param is null or undefined", () => {
        expect(isPresent(null)).toEqual(false);
        expect(isPresent(undefined)).toEqual(false);
    });
    it("returns true when the params is not null nor undefined", () => {
        expect(isPresent("text")).toEqual(true);
        expect(isPresent(0)).toEqual(true);
        expect(isPresent({})).toEqual(true);
    });
});
describe("isJsonString", () => {
    it("returns true when the string is a valid JS dump", () => {
        expect(isJsonString("1")).toEqual(true);
        expect(isJsonString("{}")).toEqual(true);
        expect(isJsonString('{"name":"roger","age":38}')).toEqual(true);
        expect(isJsonString('"2020-10-25T18:32:28.918Z"')).toEqual(true);
    });
    it("returns false when the string is NOT a valid JS dump", () => {
        expect(isJsonString({})).toEqual(false);
        expect(isJsonString({ name: "roger", age: 38 })).toEqual(false);
        expect(isJsonString(new Date())).toEqual(false);
    });
});
describe("toJsString", () => {
    it("returns the stringified version of the received param", () => {
        expect(toJsString({ name: "john", age: 19 })).toEqual('{"name":"john","age":19}');
        expect(toJsString({})).toEqual("{}");
    });
    it('returns the param as-is if it is already a valid element to be parsed', () => {
        expect(toJsString(1)).toEqual(1);
        expect('{"name":"john","age":19}').toEqual('{"name":"john","age":19}');
        expect(toJsString("{}")).toEqual("{}");
    });
});
//# sourceMappingURL=util.spec.js.map
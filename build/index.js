'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const generateFetchJsonResponse = (response, resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield response.json();
    const requestResponse = {
        headers: response.headers,
        status: response.status,
        data
    };
    if (response.ok) {
        resolve(requestResponse);
    }
    else {
        reject(requestResponse);
    }
});
const buildUrl = (host, path, params) => {
    const url = new URL(`${host}${path}`);
    // @ts-ignore
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    return url.toString();
};
class FetchManager {
    constructor(options = {}) {
        this.interceptorsBeforeRequest = [];
        this.requestConfig = {
            cache: options.cache || 'no-cache',
            credentials: options.credentials || 'same-origin',
            headers: options.headers || {},
            integrity: options.integrity,
            keepalive: Object.keys(options).includes('keepalive') ? options.keepalive : false,
            mode: options.mode || 'cors',
            redirect: options.redirect || 'follow',
            referrer: Object.keys(options).includes('referrer') ? options.referrer : 'about:client',
            referrerPolicy: options.referrerPolicy || 'no-referrer-when-downgrade',
            signal: options.signal || undefined,
            window: options.window || window
        };
        this.host = options.host || '';
    }
    beforeRequest(functionToCall) {
        this.interceptorsBeforeRequest = this.interceptorsBeforeRequest.concat(functionToCall);
    }
    options(path, options = {}) {
        return this.request('OPTIONS', path, options);
    }
    head(path, options = {}) {
        return this.request('HEAD', path, options);
    }
    get(path, options = {}) {
        return this.request('GET', path, options);
    }
    delete(path, body, options = {}) {
        const requestOptions = Object.assign({}, options, { body: body ? JSON.stringify(body) : undefined });
        return this.request('DELETE', path, requestOptions);
    }
    post(path, body, options = {}) {
        const requestOptions = Object.assign({}, options, { body: JSON.stringify(body) });
        return this.request('POST', path, requestOptions);
    }
    put(path, body, options = {}) {
        const requestOptions = Object.assign({}, options, { body: JSON.stringify(body) });
        return this.request('PUT', path, requestOptions);
    }
    patch(path, body, options = {}) {
        const requestOptions = Object.assign({}, options, { body: JSON.stringify(body) });
        return this.request('PATCH', path, requestOptions);
    }
    request(method, path, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const { params = {} } = options, fetchOptions = __rest(options, ["params"]);
            const config = Object.assign({}, this.requestConfig, fetchOptions);
            const configAfterInterceptors = this.applyBeforeInterceptors(config);
            const requestOptions = Object.assign({}, configAfterInterceptors, { method: method });
            return new Promise((resolve, reject) => {
                fetch(buildUrl(this.host, path, params), requestOptions)
                    .then((response) => generateFetchJsonResponse(response, resolve, reject))
                    .catch(reject);
            });
        });
    }
    asPromise(value) {
        return Promise.resolve(value);
    }
    applyBeforeInterceptors(initialConfig) {
        let config = initialConfig;
        this.interceptorsBeforeRequest.forEach((functionToCall) => __awaiter(this, void 0, void 0, function* () {
            config = yield this.asPromise(functionToCall(config));
        }));
        return config;
    }
}

const extractToken = (rawToken) => {
    const values = rawToken.split("Bearer");
    return (values[1] || values[0] || rawToken || "").trim();
};
const isPresent = (value) => value !== null && value !== undefined;
const isJsonString = (value) => {
    try {
        JSON.parse(value);
    }
    catch (e) {
        return false;
    }
    return true;
};
const toJsString = (value) => isJsonString(value) || typeof value === 'string' ? value : JSON.stringify(value);

const DEFAULT_KEY_PREFIX = "";
class Storage {
    constructor(container, keyPrefix) {
        this.container = container;
        this.keyPrefix = keyPrefix || DEFAULT_KEY_PREFIX;
    }
    get(key, defaultValue = undefined) {
        if (!this.hasKey(key))
            return defaultValue;
        const value = this.container.getItem(this.prefixedKey(key)) || "";
        try {
            return JSON.parse(value);
        }
        catch (e) {
            return value;
        }
    }
    set(key, value) {
        if (typeof key === 'string' && isPresent(value)) {
            this.addItem(this.prefixedKey(key), toJsString(value));
        }
    }
    keys() {
        return Object.keys(this.container);
    }
    size() {
        return this.container.length;
    }
    hasKey(key) {
        return this.container.hasOwnProperty(this.prefixedKey(key));
    }
    delete(key) {
        this.container.removeItem(this.prefixedKey(key));
    }
    clear() {
        return this.container.clear();
    }
    prefixedKey(key) {
        return this.keyPrefix.length > 0 ? `${this.keyPrefix}.${key}` : key.toString();
    }
    addItem(key, value) {
        this.container.setItem(key, value.toString());
    }
}
Storage.local = (keyPrefix = DEFAULT_KEY_PREFIX) => new Storage(localStorage, keyPrefix);
Storage.session = (keyPrefix = DEFAULT_KEY_PREFIX) => new Storage(sessionStorage, keyPrefix);

exports.FetchManager = FetchManager;
exports.Storage = Storage;
exports.extractToken = extractToken;
exports.isJsonString = isJsonString;
exports.isPresent = isPresent;
exports.toJsString = toJsString;
//# sourceMappingURL=index.js.map

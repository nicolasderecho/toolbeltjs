import { rest } from 'msw';
declare const server: {
    listen(options?: import("msw/lib/types/sharedOptions").SharedOptions | undefined): void;
    use(...handlers: import("msw/lib/types/setupWorker/glossary").RequestHandlersList): void;
    restoreHandlers(): void;
    resetHandlers(...nextHandlers: import("msw/lib/types/setupWorker/glossary").RequestHandlersList): void;
    close(): void;
};
declare const stubOptionsRequest: (url: string | RegExp, jsonResponse?: object, httpStatus?: HttpStatus) => void;
declare const stubHeadRequest: (url: string | RegExp, jsonResponse?: object, httpStatus?: HttpStatus) => void;
declare const stubDeleteRequest: (url: string | RegExp, jsonResponse?: object, httpStatus?: HttpStatus) => void;
declare const stubPatchRequest: (url: string | RegExp, jsonResponse: object, httpStatus?: HttpStatus) => void;
declare const stubPutRequest: (url: string | RegExp, jsonResponse: object, httpStatus?: HttpStatus) => void;
declare const stubPostRequest: (url: string | RegExp, jsonResponse: object, httpStatus?: HttpStatus) => void;
declare const stubGetRequest: (url: string | RegExp, jsonResponse: object, httpStatus?: HttpStatus) => void;
declare const stubRequest: (httpMethod: HttpMethod, url: string | RegExp, jsonResponse: object, httpStatus?: HttpStatus) => void;
declare const stubNetworkError: (url: string | RegExp) => void;
export { server, rest, stubRequest, stubNetworkError, stubPostRequest, stubGetRequest, stubPutRequest, stubOptionsRequest, stubHeadRequest, stubPatchRequest, stubDeleteRequest };

import { rest } from 'msw';
import { setupServer } from 'msw/node';
const server = setupServer(rest.get('/api/example', (req, res, ctx) => {
    return res(ctx.json([1]));
}), rest.get('/api/second/example', (req, res, ctx) => {
    return res(ctx.json([2]));
}));
const stubOptionsRequest = (url, jsonResponse = {}, httpStatus = { code: 200, text: 'options' }) => stubRequest('options', url, jsonResponse, httpStatus);
const stubHeadRequest = (url, jsonResponse = {}, httpStatus = { code: 200, text: 'head' }) => stubRequest('head', url, jsonResponse, httpStatus);
const stubDeleteRequest = (url, jsonResponse = {}, httpStatus = { code: 200, text: 'deleted' }) => stubRequest('delete', url, jsonResponse, httpStatus);
const stubPatchRequest = (url, jsonResponse, httpStatus = { code: 200, text: 'updated' }) => stubRequest('patch', url, jsonResponse, httpStatus);
const stubPutRequest = (url, jsonResponse, httpStatus = { code: 200, text: 'updated' }) => stubRequest('put', url, jsonResponse, httpStatus);
const stubPostRequest = (url, jsonResponse, httpStatus = { code: 201, text: 'created' }) => stubRequest('post', url, jsonResponse, httpStatus);
const stubGetRequest = (url, jsonResponse, httpStatus = { code: 200, text: 'ok' }) => stubRequest('get', url, jsonResponse, httpStatus);
const stubRequest = (httpMethod, url, jsonResponse, httpStatus = { code: 200, text: 'ok' }) => {
    const methodToCall = rest[httpMethod].bind(rest);
    return server.use(methodToCall(url, (req, res, ctx) => {
        return res(ctx.status(httpStatus.code, httpStatus.text), ctx.json(jsonResponse));
    }));
};
const stubNetworkError = (url) => server.use(rest.get(url, (req, res, ctx) => {
    return res.networkError('Failed to connect');
}));
export { server, rest, stubRequest, stubNetworkError, stubPostRequest, stubGetRequest, stubPutRequest, stubOptionsRequest, stubHeadRequest, stubPatchRequest, stubDeleteRequest };
//# sourceMappingURL=fakeServer.js.map
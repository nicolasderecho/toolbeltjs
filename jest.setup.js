// const fetchMock = require("jest-fetch-mock");
//
// fetchMock.enableMocks();
//
// global.stubRequest = (urlOrPredicate, jsonResponse) => fetchMock.mockIf(urlOrPredicate, JSON.stringify(jsonResponse));

const { server } = require('./support/fakeServer'); //require http fake server
require('whatwg-fetch'); //polyfill fetch

const originalFetch = fetch;
jest.spyOn(global, 'fetch').mockImplementation(originalFetch) //spy fetch before calling it


global.raisePromiseShouldHaveFailedError = () => fail('A previous promise was supposed to fail but it succeed.');

beforeAll(() => {
  server.listen();
});
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

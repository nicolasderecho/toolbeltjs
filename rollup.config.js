import typescript from "rollup-plugin-typescript2";
import commonjs from "rollup-plugin-commonjs";
import external from "rollup-plugin-peer-deps-external";
import resolve from "rollup-plugin-node-resolve";
import pkg from "./package.json";
import { terser } from "rollup-plugin-terser";
export default {
  input: "src/index.ts", // our source file
  output: [
    {
      file: pkg.main,
      format: "cjs",
      exports: "named",
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: "es", // the preferred format,
      exports: "named",
      sourcemap: true,
    },
    {
      file: pkg.browser,
      format: "iife",
      exports: "named",
      sourcemap: true,
      name: "ToolbeltJS", // the global which can be used in a browser
    },
  ],
  external: [...Object.keys(pkg.dependencies || {}), ...Object.keys(pkg.peerDependencies || {})],
  plugins: [
    external(),
    resolve(),
    typescript({
      typescript: require("typescript"),
      rollupCommonJSResolveHack: true,
    }),
    commonjs({
      include: ["node_modules/**"],
    }),
    //terser(), // minifies generated bundles
  ],
};

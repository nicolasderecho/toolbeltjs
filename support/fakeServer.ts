import { rest } from 'msw';
import { setupServer } from 'msw/node';

const server = setupServer(
    rest.get('/api/example', (req, res, ctx) => {
        return res(ctx.json([1]));
    }),
    rest.get('/api/second/example', (req, res, ctx) => {
        return res(ctx.json([2]));
    })
);

const stubOptionsRequest = (url: string | RegExp, jsonResponse: object = {}, httpStatus: HttpStatus = { code: 200, text: 'options'}) => stubRequest('options', url, jsonResponse, httpStatus);
const stubHeadRequest = (url: string | RegExp, jsonResponse: object = {}, httpStatus: HttpStatus = { code: 200, text: 'head'}) => stubRequest('head', url, jsonResponse, httpStatus);
const stubDeleteRequest = (url: string | RegExp, jsonResponse: object = {}, httpStatus: HttpStatus = { code: 200, text: 'deleted'}) => stubRequest('delete', url, jsonResponse, httpStatus);
const stubPatchRequest = (url: string | RegExp, jsonResponse: object, httpStatus: HttpStatus = { code: 200, text: 'updated'}) => stubRequest('patch', url, jsonResponse, httpStatus);
const stubPutRequest = (url: string | RegExp, jsonResponse: object, httpStatus: HttpStatus = { code: 200, text: 'updated'}) => stubRequest('put', url, jsonResponse, httpStatus);
const stubPostRequest = (url: string | RegExp, jsonResponse: object, httpStatus: HttpStatus = { code: 201, text: 'created'}) => stubRequest('post', url, jsonResponse, httpStatus);

const stubGetRequest = (url: string | RegExp, jsonResponse: object, httpStatus: HttpStatus = { code: 200, text: 'ok'}) => stubRequest('get', url, jsonResponse, httpStatus);

const stubRequest = (httpMethod: HttpMethod, url: string | RegExp, jsonResponse: object, httpStatus: HttpStatus = { code: 200, text: 'ok'}) => {
    const methodToCall = rest[httpMethod].bind(rest);
    return server.use(
        methodToCall(url, (req, res, ctx) => {
            return res(
                ctx.status(httpStatus.code, httpStatus.text),
                ctx.json(jsonResponse)
            );
        })
    );
}

const stubNetworkError = (url: string | RegExp) => server.use(
    rest.get(url, (req, res, ctx) => {
        return res.networkError('Failed to connect');
    })
);

export { server, rest, stubRequest, stubNetworkError, stubPostRequest, stubGetRequest, stubPutRequest, stubOptionsRequest, stubHeadRequest, stubPatchRequest, stubDeleteRequest };